package templa.master.templateproject.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import templa.master.templateproject.ui.fragment.PhotoFragment

class PagerAdapter(fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager) {

    companion object {
        private const val FRAGMENT_1 = 0
        private const val FRAGMENT_2 = 1
        private const val FRAGMENT_3 = 2
        private const val FRAGMENT_4 = 3
        private const val FRAGMENT_5 = 4
        private const val FRAGMENT_COUNT = 5
    }

    override fun getItem(pos: Int): Fragment {
        return when (pos) {
            FRAGMENT_1 -> PhotoFragment.newInstance("1")
            FRAGMENT_2 -> PhotoFragment.newInstance("2")
            FRAGMENT_3 -> PhotoFragment.newInstance("3")
            FRAGMENT_4 -> PhotoFragment.newInstance("4")
            FRAGMENT_5 -> PhotoFragment.newInstance("5")
            else -> throw UnsupportedOperationException("Wrong position")
        }
    }

    override fun getCount() = FRAGMENT_COUNT
}