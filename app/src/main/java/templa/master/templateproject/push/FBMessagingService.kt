package templa.master.templateproject.push

import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.support.v4.app.NotificationCompat
import templa.master.templateproject.R
import templa.master.templateproject.ui.WelcomeActivity


class FBMessagingService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        super.onMessageReceived(remoteMessage)
        val title = remoteMessage?.notification?.title ?: ""
        val message = remoteMessage?.notification?.body ?: ""

        showNotifications(title, message)
    }

    private fun showNotifications(title: String, msg: String) {
        val i = Intent(this, WelcomeActivity::class.java)

        val pendingIntent = PendingIntent.getActivity(
            this, 100,
            i, PendingIntent.FLAG_UPDATE_CURRENT
        )

        val notification = NotificationCompat.Builder(this)
            .setContentText(msg)
            .setContentTitle(title)
            .setContentIntent(pendingIntent)
            .setSmallIcon(R.mipmap.ic_launcher)
            .build()

        val manager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        manager.notify(100, notification)
    }
}


