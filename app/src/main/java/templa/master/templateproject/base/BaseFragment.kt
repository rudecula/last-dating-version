package templa.master.templateproject.base

import android.app.ProgressDialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import templa.master.templateproject.R


abstract class BaseFragment : Fragment() {

    private lateinit var mProgressDialog: ProgressDialog

    abstract fun getLayoutId(): Int

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(getLayoutId(), container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initProgressDialog()
        initUi()
    }

    private fun initProgressDialog() {
        mProgressDialog = ProgressDialog(requireContext())
        mProgressDialog.setMessage(getString(R.string.please_wait))
        mProgressDialog.setCancelable(false)
    }

    open fun initUi() {}


}