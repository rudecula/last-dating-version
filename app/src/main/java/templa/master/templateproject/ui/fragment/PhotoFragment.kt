package templa.master.templateproject.ui.fragment

import android.os.Bundle
import kotlinx.android.synthetic.main.fragment_photo.*
import templa.master.templateproject.R
import templa.master.templateproject.base.BaseFragment

class PhotoFragment : BaseFragment() {
    companion object {
        const val ID_BUNDLE = "id"
        fun newInstance(id: String): PhotoFragment {
            val bundle = Bundle()
            bundle.putString(ID_BUNDLE, id)
            val fragment = PhotoFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun getLayoutId(): Int = R.layout.fragment_photo

    override fun initUi() {
        super.initUi()
        val id = arguments?.getString(ID_BUNDLE) ?: "1"

        val res = when (id) {
            "1" -> R.drawable.ic_1
            "2" -> R.drawable.ic_2
            "3" -> R.drawable.ic_3
            "4" -> R.drawable.ic_4
            "5" -> R.drawable.ic_5
            else -> R.drawable.ic_1
        }
        ivPhoto.setImageResource(res)
    }
}