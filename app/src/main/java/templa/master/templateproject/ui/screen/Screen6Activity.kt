package templa.master.templateproject.ui.screen

import android.content.Context
import android.content.Intent
import android.os.Bundle
import templa.master.templateproject.R
import templa.master.templateproject.base.BaseActivity

class Screen6Activity : BaseActivity() {
    companion object {
        fun getStartIntent(context: Context): Intent = Intent(context, Screen6Activity::class.java)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_screen_6)
    }
}