package templa.master.templateproject.ui.screen

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import templa.master.templateproject.R
import templa.master.templateproject.base.BaseActivity

class Screen4Activity : BaseActivity(), View.OnClickListener {
    companion object {
        fun getStartIntent(context: Context): Intent = Intent(context, Screen4Activity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_screen_4)
    }

    override fun onClick(v: View?) {
        startActivity(Screen5_1Activity.getStartIntent(this))
    }
}