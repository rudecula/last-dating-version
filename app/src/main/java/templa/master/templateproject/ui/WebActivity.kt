package templa.master.templateproject.ui

import android.Manifest
import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.webkit.*
import kotlinx.android.synthetic.main.activity_web.*
import templa.master.templateproject.R
import templa.master.templateproject.base.BaseActivity
import android.support.v4.app.ActivityCompat
import android.content.pm.PackageManager
import android.net.Uri
import android.support.v4.content.ContextCompat
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import android.support.annotation.NonNull
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.widget.Toast
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class WebActivity : BaseActivity() {
    companion object {
        private const val URL_ADDRESS_INTENT = "url_address"
        const val SAVED_URL = "savedUrl"
        fun getStartIntent(context: Context, url: String): Intent {
            val intent = Intent(context, WebActivity::class.java)
            intent.putExtra(this.URL_ADDRESS_INTENT, url)
            return intent
        }
    }

    private val TAG = WebActivity::class.java.simpleName
    var currentUrl: String? = ""
    private val PICK_FROM_GALLERY = 1
    private lateinit var mPref: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web)
        mPref = getSharedPreferences("presName", Context.MODE_PRIVATE)

        if (Build.VERSION.SDK_INT >= 23 && (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED)
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA),
                1
            )
        }

        currentUrl = intent.getStringExtra(URL_ADDRESS_INTENT)
        CookieSyncManager.createInstance(this)
        checkPermissions()
        initWebView()
    }

    private fun checkPermissions() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE),
                PICK_FROM_GALLERY
            )
        }
    }


    private fun initWebView() {
        webView.webChromeClient = object : WebChromeClient() {

            override fun onProgressChanged(view: WebView?, newProgress: Int) {
                super.onProgressChanged(view, newProgress)
                progress.progress = newProgress
                if (newProgress == 100) {
                    progress.visibility = View.GONE
                } else {
                    progress.visibility = View.VISIBLE
                }
            }

            fun openFileChooser(uploadMsg: ValueCallback<Uri>, acceptType: String, capture: String) {
                mUM = uploadMsg
                val i = Intent(Intent.ACTION_GET_CONTENT)
                i.addCategory(Intent.CATEGORY_OPENABLE)
                i.type = "*/*"
                startActivityForResult(Intent.createChooser(i, "File Chooser"), FCR)
            }

            //For Android 5.0+
            override fun onShowFileChooser(
                webView: WebView, filePathCallback: ValueCallback<Array<Uri>>,
                fileChooserParams: WebChromeClient.FileChooserParams
            ): Boolean {
                mUMA?.onReceiveValue(null)
                mUMA = filePathCallback
                var takePictureIntent: Intent? = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                if (takePictureIntent?.resolveActivity(packageManager) != null) {
                    var photoFile: File? = null
                    try {
                        photoFile = createImageFile()
                        takePictureIntent.putExtra("PhotoPath", mCM)
                    } catch (ex: IOException) {
                        Log.e(TAG, "Image file creation failed", ex)
                    }

                    if (photoFile != null) {
                        mCM = "file:" + photoFile.absolutePath
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile))
                    } else {
                        takePictureIntent = null
                    }
                }
                val contentSelectionIntent = Intent(Intent.ACTION_GET_CONTENT)
                contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE)
                contentSelectionIntent.type = "*/*"
                val intentArray: Array<Intent>
                if (takePictureIntent != null) {
                    intentArray = arrayOf(takePictureIntent)
                } else {
                    intentArray = arrayOf(Intent())
                }

                val chooserIntent = Intent(Intent.ACTION_CHOOSER)
                chooserIntent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent)
                chooserIntent.putExtra(Intent.EXTRA_TITLE, "Image Chooser")
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray)
                startActivityForResult(chooserIntent, FCR)
                return true
            }

            override fun onJsAlert(view: WebView?, url: String?, message: String?, result: JsResult?): Boolean {
                Log.d("LogTag", message)
                result?.confirm()
                return true
            }
        }

        webView.webViewClient = object : WebViewClient() {

            @TargetApi(Build.VERSION_CODES.N)
            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                val uri = request?.url
                if (!uri?.queryParameterNames.isNullOrEmpty()) {
                    mPref.edit().putString(SAVED_URL, uri.toString()).apply()
                    Log.i("Saved link: ", uri.toString())
                }
                return shouldOverrideUrlLoading(uri.toString())
            }

            @Deprecated("shouldOverrideUrlLoading")
            override fun shouldOverrideUrlLoading(view: WebView, url: String?): Boolean {
                val uri = Uri.parse(url)
                if (!uri?.queryParameterNames.isNullOrEmpty()) {
                    mPref.edit().putString(SAVED_URL, uri.toString()).apply()
                    Log.i("Saved link: ", uri.toString())
                }
                return shouldOverrideUrlLoading(url ?: "")
            }

            private fun shouldOverrideUrlLoading(url: String): Boolean {
                if (url.startsWith("market")) {
                    startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
                    finish()
                    return true
                }
                currentUrl = url
                val uri = Uri.parse(url)
                if (!uri?.queryParameterNames.isNullOrEmpty()) {
                    mPref.edit().putString(SAVED_URL, uri.toString()).apply()
                    Log.i("Saved link: ", uri.toString())
                }
                Log.d("WebView", "get Url $currentUrl")
//                load()
                return false
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                val cookieManager = CookieManager.getInstance()

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    cookieManager.setAcceptThirdPartyCookies(webView, true)
                } else {
                    cookieManager.setAcceptCookie(true)
                }

                CookieSyncManager.getInstance().startSync()
                CookieSyncManager.getInstance().sync()

                super.onPageFinished(view, url)
            }
        }
        webView.clearCache(true)
        webView.clearHistory()
        webView.setInitialScale(1)
        webView.settings.javaScriptEnabled = true
        webView.settings.loadWithOverviewMode = true
        webView.settings.useWideViewPort = true
        webView.settings.javaScriptCanOpenWindowsAutomatically = true
        webView.scrollBarStyle = WebView.SCROLLBARS_OUTSIDE_OVERLAY
        webView.isScrollbarFadingEnabled = false
        webView.settings.builtInZoomControls = true
        webView.settings.displayZoomControls = false
        webView.settings.domStorageEnabled = true
        load()
    }

    private fun load() {
        webView.loadUrl(currentUrl)
    }

    override fun onBackPressed() {
        if (webView.canGoBack()) {
            val backForwardList = webView.copyBackForwardList()
            val historyUrl = backForwardList.getItemAtIndex(backForwardList.currentIndex - 1).url
            if (historyUrl != currentUrl) {
                webView.goBack()
            } else {
                finish()
            }
        } else {
            finish()
        }
    }

    private var mCM: String? = null
    private var mUM: ValueCallback<Uri>? = null
    private var mUMA: ValueCallback<Array<Uri>>? = null
    private val FCR = 1

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)
        if (Build.VERSION.SDK_INT >= 21) {
            var results: Array<Uri>? = null
            //Check if response is positive
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == FCR) {
                    if (null == mUMA) {
                        return
                    }
                    if (intent == null) {
                        //Capture Photo if no image available
                        if (mCM != null) {
                            results = arrayOf(Uri.parse(mCM))
                        }
                    } else {
                        val dataString = intent.dataString
                        if (dataString != null) {
                            results = arrayOf(Uri.parse(dataString))
                        }
                    }
                }
            }
            mUMA?.onReceiveValue(results)
            mUMA = null
        } else {
            if (requestCode == FCR) {
                if (null == mUM) return
                val result = if (intent == null || resultCode != Activity.RESULT_OK) null else intent.data
                mUM?.onReceiveValue(result)
                mUM = null
            }
        }
    }

    // Create an image file
    @Throws(IOException::class)
    private fun createImageFile(): File {
        @SuppressLint("SimpleDateFormat") val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "img_" + timeStamp + "_"
        val storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(imageFileName, ".jpg", storageDir)
    }

    override fun onKeyDown(keyCode: Int, @NonNull event: KeyEvent): Boolean {
        if (event.action == KeyEvent.ACTION_DOWN) {
            when (keyCode) {
                KeyEvent.KEYCODE_BACK -> {
                    if (webView.canGoBack()) {
                        val backForwardList = webView.copyBackForwardList()
                        val historyUrl = backForwardList.getItemAtIndex(backForwardList.currentIndex - 1).url
                        if (historyUrl != currentUrl) {
                            webView.goBack()
                        } else {
                            finish()
                        }
                    } else {
                        finish()
                    }
                    return true
                }
            }
        }
        return super.onKeyDown(keyCode, event)
    }
}