package templa.master.templateproject.model

import com.google.gson.annotations.SerializedName

data class IPDetect(
    @SerializedName("city")
    var city: String,
    @SerializedName("country")
    var country: String,
    @SerializedName("ip")
    var ip: String,
    @SerializedName("loc")
    var loc: String,
    @SerializedName("org")
    var org: String,
    @SerializedName("postal")
    var postal: String,
    @SerializedName("region")
    var region: String
)