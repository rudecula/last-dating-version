package templa.master.templateproject.network

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url
import templa.master.templateproject.model.IPDetect


interface Api {
    @GET
    fun checkIpAddress(
        @Url url: String = "https://ipinfo.io/json",
        @Query("token") input: String = "9749e61fd50ca6"
    ): Single<IPDetect>

//    @GET
//    fun placeAutoCompleteCall(
//        @Url url: String = "https://maps.googleapis.com/maps/api/place/findplacefromtext/json",
//        @Query("input") input: String,
//        @Query("inputtype") inputtype: String = "textquery",
//        @Query("fields") fields: String = "formatted_address,place_id",
//        @Query("key") key: String = KEY_GOOGLE_PLACES,
//        @Query("language") language: String = "ru"
//    ): Single<PlacesResponce>
//
//    @GET
//    fun placeDetailsCall(
//        @Url url: String = "https://maps.googleapis.com/maps/api/place/details/json",
//        @Query("placeid") placeid: String,
//        @Query("key") key: String = KEY_GOOGLE_PLACES,
//        @Query("language") language: String = "ru"
//    ): Single<DetailsPlacesResponce>
}